/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/09 14:22:40 by akazian           #+#    #+#             */
/*   Updated: 2014/02/09 18:49:47 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <client.h>

void	ft_error(char *error)
{
	ft_putstr_fd("MiniTalk CLIENT: ", 2);
	ft_putendl_fd(error, 2);
	exit(0);
}

int	main(int argc, char **argv)
{
	if (argc != 3)
	{
		ft_error("usage: pid server, message");
		return (0);
	}
	send_mess(argv[1], argv[2]);
	return (0);
}
