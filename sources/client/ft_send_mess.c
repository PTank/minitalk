/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_send_mess.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/09 14:22:30 by akazian           #+#    #+#             */
/*   Updated: 2014/02/09 18:08:39 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <client.h>

void	char_to_sig(char mess, int pid)
{
	char	c;

	c = 0;
	while (c < 8)
	{
		usleep(500);
		if (((mess >> c) & 1) == 1)
		{
			if (kill(pid, SIGUSR1) == -1)
				ft_error("signal error");
		}
		else
		{
			if (kill(pid, SIGUSR2))
				ft_error("signal error");
		}
		c++;
	}
}

void	send_mess(char *pid, char *mess)
{
	int	s_pid;
	int	i;

	s_pid = ft_atoi(pid);
	i = 0;
	while(mess[i] != '\0')
	{
		char_to_sig(mess[i], s_pid);
		i++;
	}
	char_to_sig(mess[i], s_pid);
}
