/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/07 19:38:44 by akazian           #+#    #+#             */
/*   Updated: 2014/02/09 18:49:23 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <server.h>

void	ft_error(char *error)
{
	ft_putstr_fd("MiniTalk SERVER: ", 2);
	ft_putendl_fd(error, 2);
	exit(0);
}

int	main(void)
{
	pid_t	server;

	server = getpid();
	ft_putstr("\33[32mserver pid is: ");
	ft_putnbr(server);
	ft_putchar('\n');
	ft_putendl("-------------------\33[0m");
	ft_catch_sig();
	while (42)
		pause();
	return (0);
}
