/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_catch_sig.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/07 19:59:09 by akazian           #+#    #+#             */
/*   Updated: 2014/02/09 18:42:31 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <server.h>

static void	end_str(char *str)
{
	if (str)
		ft_putendl(str);
	else
		ft_putchar('\n');
	ft_strdel(&str);
	str = NULL;
}

static void	print_char(int bin)
{
	static int	i = 0;
	static char	c = 0;
	static char	*str = NULL;
	char		*tmp;

	c += (bin << i++);
	if (i > 7)
	{
		if (c == '\0')
			end_str(str);
		else
		{
			if (str == NULL)
				str = ft_strdup(&c);
			else
			{
				tmp = ft_strjoin(str, &c);
				ft_strdel(&str);
				str = tmp;
			}
		}
		c = 0;
		i = 0;
	}
}

static void	test_sig(int numsig)
{
	if (numsig == SIGUSR1)
		print_char(1);
	if (numsig == SIGUSR2)
		print_char(0);
}

void		ft_catch_sig(void)
{
	usleep(500);
	if (signal(SIGUSR1, test_sig) == SIG_ERR)
		ft_error("signal error");
	if (signal(SIGUSR2, test_sig) == SIG_ERR)
		ft_error("signal error");
}
