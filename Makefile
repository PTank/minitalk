# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: akazian <akazian@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 16:00:35 by akazian           #+#    #+#              #
#    Updated: 2014/02/09 18:15:18 by akazian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	=	minitalk
LIBFT	=	libft
SERVER	=	sources/server
CLIENT	=	sources/client
MAKE	=	make -C
MAKEFCLEAN =	make fclean -C
MAKECLEAN =	make clean -C

all	:	$(NAME)

$(NAME)	:
	$(MAKE) $(LIBFT)
	$(MAKE) $(CLIENT)
	$(MAKE) $(SERVER)

.PHONY: clean all re fclean

fclean	:	clean
	$(MAKEFCLEAN) $(CLIENT)
	$(MAKEFCLEAN) $(SERVER)

clean	:
	$(MAKECLEAN) $(CLIENT)
	$(MAKECLEAN) $(SERVER)

re	:	fclean all

