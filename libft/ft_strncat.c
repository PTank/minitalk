/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 20:14:01 by akazian           #+#    #+#             */
/*   Updated: 2013/11/28 10:24:21 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	char	*dest;

	dest = s1;
	while (*dest != '\0')
		dest++;
	while (*s2 != '\0' && n--)
	{
		*dest = *s2;
		dest++;
		s2++;
	}
	*dest = '\0';
	return (s1);
}
