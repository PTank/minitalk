/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 17:06:38 by akazian           #+#    #+#             */
/*   Updated: 2013/12/01 20:11:55 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*fstr;
	size_t	i;

	i = 0;
	if (s && f)
	{
		fstr = ft_strnew(ft_strlen(s));
		while (s[i] != '\0')
		{
			fstr[i] = f(i, s[i]);
			i++;
		}
	}
	else
		return (NULL);
	return (fstr);
}
