/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/09 14:22:15 by akazian           #+#    #+#             */
/*   Updated: 2014/02/09 14:22:18 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _CLIENT_H
# define _CLIENT_H

# include <libft.h>
# include <unistd.h>
# include <stdlib.h>
# include <signal.h>

void	ft_error(char *error);
void	char_to_sig(char mess, int pid);
void	send_mess(char *pid, char *mess);

#endif
