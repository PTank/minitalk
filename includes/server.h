/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/07 19:39:06 by akazian           #+#    #+#             */
/*   Updated: 2014/02/07 20:24:07 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _SERVER_H
# define _SERVER_H

# include <libft.h>
# include <unistd.h>
# include <stdlib.h>
# include <signal.h>

void	ft_catch_sig(void);
void	ft_error(char *error);

#endif
